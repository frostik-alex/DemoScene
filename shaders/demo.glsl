precision lowp float;

uniform float time;
uniform vec2 resolution;
uniform sampler2D customTex1;
uniform sampler2D customTex2;
uniform vec2 mousePos;

const float PI = 3.141592653589793238;

const vec3 BG_COLOR = vec3( 0.2, 0.3, 0.5);


#define MAX_STEPS 200
#define FAR_PLANE 100.0


mat3 getVecRotation( vec3 a, float angle){
    float s = sin(angle), c = cos(angle);
    return mat3(
        vec3( c + a.x*a.x*(1.0 - c),   a.x*a.y*(1.0-c) - a.z*s,  a.x*a.z*(1.0-c)+a.y*s ),
        vec3( a.y*a.x*(1.0-c)+a.z*s,  c+a.y*a.y*(1.0-c),        a.y*a.z*(1.0-c)-a.x*s ),
        vec3( a.z*a.x*(1.0-c)-a.y*s,  a.z*a.y*(1.0-c)+a.x*s,    c + a.z*a.z*(1.0-c)   )
    );
}

float hash2(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
float shSin(float x){
    return (sin(x) + 1.0) / 2.0;
}
vec4 opU(vec4 o1, vec4 o2){
    return o1.x <= o2.x ? o1 : o2;
}
vec4 opU(vec4 o1, vec2 o2){
    return o1.x <= o2.x ? o1 : vec4(o2, vec2(0.0));
}

vec4 opMix(vec4 o1, vec4 o2, float a){
    return vec4( mix(o1.x, o2.x, a), o1.y, o1.zw); //fail function - do not use
}

vec3 box(vec2 p, vec4 boxProp){
    vec3 res = vec3(0.0);

    vec2 texCoords = vec2(0.0);


    texCoords.x = (p.x - boxProp.x)/boxProp.z;
    texCoords.y = -(p.y - boxProp.y)/boxProp.w;

    res.xy = texCoords;
    res.z = step(p.x, boxProp.x + boxProp.z)*step(boxProp.x, p.x)*step(p.y, boxProp.y + boxProp.w)*step(boxProp.y, p.y);

    return res;
}
float singleCircle( vec2 p, vec3 cProp ){
    return step(
        (p.x - cProp.x)*(p.x - cProp.x) + (p.y - cProp.y)*(p.y - cProp.y),
        cProp.z*cProp.z
    );
}
vec3 smallBulb(vec2 p, float t, float cNum, float cS){
    vec3 res = vec3(0.0);

    float angle = atan(p.y, p.x);
    float dist = cNum * cS + 0.5 * cS + t;

    float as = PI/10.0; //angle step

    float angV = t / PI * 6.0 * ( (mod(floor(cNum/2.0), 2.0)-0.5)*2.0 );

    float ap = floor((angle + angV)/as)*as + as*0.5 - angV; //angular position

    vec2 cp = vec2( cos(ap), sin(ap) ) * dist;


    float bulbRadius = 0.1;

    vec2 texCoords = ((p - cp) / bulbRadius + 1.0)/2.0;
    texCoords.y *= -1.0;
    res = vec3( texCoords, step(length(p - cp), bulbRadius) );

    return res;
}
vec3 coloredCircle(vec2 p, float t){
    vec3 color = vec3(0.0);

    vec3 bulbData = vec3(0.0);

    float d = length(p) - t; //distance
    float sW = 0.2; //strip width

    float cNum = floor(d/sW);

    float cDist = mod( cNum, 2.0); //color distribution 0-color1, 1-color2


    bulbData = smallBulb(p, t, cNum, sW);
    float bA = (1.0 - cDist) * bulbData.z; // bulb amount

    vec3 c1 = vec3( 0.7, 0.0, 0.0 ); //color 1
    vec3 c2 = vec3( 0.8, 0.6, 0.0 ); //color 2


//    vec3 bc = vec3( 0.99, 0.96, 0.05 ); //bulb color
    vec4 bc = texture2D( customTex1, bulbData.xy );
    bA *= bc.w;

    color = mix(c1, c2, cDist);
    color = mix(color, bc.xyz, bA);


    return color;
}
mat2 getRotMat2d(float angle){
    return mat2(
        vec2(cos(angle), -sin(angle) ),
        vec2(sin(angle), cos(angle) )
    );
}
vec3 scene(vec2 p, float t){
    vec3 color = vec3(0.5);

//    vec2 uv = -1.0+2.0*p;

    float cT = time * 0.06;

    color = coloredCircle(p, cT);

    float rt = time * 0.4;
    mat2 rot = getRotMat2d( -PI/3.0 + (PI - PI/3.0)*(sin(rt) + 1.0)/2.0 );

    vec2 rP = p*rot;

    vec3 sunData = box(rP, vec4(-0.47, -0.5, 1.0, 1.0));

    vec4 sunColor = texture2D( customTex2, sunData.xy );
    float sA = sunData.z * sunColor.w;
    color = mix(color, sunColor.xyz, sA);

    return color;
}


float objSphere( vec3 p, vec3 sp, float r ){
    return length( p - sp ) - r;
}

float objBox(vec3 p, vec3 bp, vec3 bs){
    vec3 d = abs(p - bp) - bs;
    return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}

/*
    Returns materials:
        matPos - material of positive sides: front, top, right
        matNeg - material of negative sides: back, bottom, left
*/

vec4 objTexBox(vec3 p, vec3 bp, vec3 bs, vec3 matPos, vec3 matNeg){
    vec3 d = abs(p - bp) - bs;

    float s = max(d.x,max(d.y,d.z));
    float dist = min(s,0.0) + length(max(d,0.0));

    vec2 texCoords = vec2(0.0);
    float material = 0.0;

    float sideX = (d.x == s? 1.0: 0.0);
    material += sideX * (sign(p.x - bp.x) > 0.0 ? matPos.x : matNeg.x );
    texCoords += sideX * (p - bp).zy / bs.zy;

    float sideY = (d.y == s? 1.0: 0.0);
    material += sideY * (sign(p.y - bp.y) > 0.0 ? matPos.y : matNeg.y );
    texCoords += sideY * (p - bp).xz / bs.xz;

    float sideZ = (d.z == s? 1.0: 0.0);
    material += sideZ * (sign(p.z - bp.z) > 0.0 ? matPos.z : matNeg.z );
    texCoords += sideZ * (p - bp).xy / bs.xy;

    return vec4( dist, material, texCoords );
}

float objPlaneXZ( vec3 p ){
    return p.y + 1.0;
}
float objPlaneYZ( vec3 p ){
    return p.x + 2.0 ;
}
float objPlaneNP(vec3 p,  vec3 n, vec3 r ){ //n - normal to a plane, r - point of a plane
     float a = n.x;
     float b = n.y;
     float c = n.z;
     float d = -n.x*r.x - n.y*r.y - n.z*r.z;
     return abs( a*p.x + b*p.y + c*p.z + d)/sqrt(a*a + b*b + c*c);
}
float objPlaneNPCircle(vec3 p,  vec3 n, vec3 r, float cr){ //n - normal to a plane, r - point of a plane, cr - circle radius
    float a = n.x;
    float b = n.y;
    float c = n.z;

    float d = -n.x*r.x - n.y*r.y - n.z*r.z;
    float dist = abs( a*p.x + b*p.y + c*p.z + d)/sqrt(a*a + b*b + c*c);
    vec3 p1 = p - n * dist;
    vec3 p2 = r + normalize( p1 - r ) * cr;

    return  length( p1 - r ) < cr ? dist : length( p - p2 );
}

vec4 map( vec3 p ){
    vec4 res = vec4(FAR_PLANE, -1.0, 0.0, 0.0);

    vec3 distortion = vec3(0.5* sin(44.0), 0.7 * sin(12.0), 0.2 * sin(44.4));
    float T = time * 0.1;//* (randN * 0.6 + 0.4)  + randN * 60.0;
    float d = 0.1 * ( shSin(p.z * 8.0 + T ) + shSin( p.x * 11.0  + T ) + shSin( p.y * 9.5  + T ));
    res = opU( res, vec2( objSphere( p , vec3( 1.0, 0.7, 0.1 ), 0.2 ), 123.0 ) );

    res = opU( res, vec2( objSphere( p, vec3( 0.0, 0.0, 0.0 ), 0.5 ), 65.0 ) );

    res = opU( res, vec2( objPlaneXZ(p), 333.0));
    res = opU( res, vec2( objSphere( p, vec3( -1.0, 0.5, 0.1 ), 0.2 ), 33.0 ) );

    vec4 morphSphere = vec4(vec2( objSphere( p, vec3( 1.5, 0.3, 0.0 ), 0.5 ), 77.0 ), vec2(0.0));
    vec4 boxProp = objTexBox(
        p, vec3( 1.5, 0.3, 0.0 ),
        vec3( 0.1, 1.0, 1.0 ),
        vec3(1488.0, 20.0, 23.0),
        vec3(1488.0, 50.0, 566.0)
    );
    vec4 mixResult = opMix( boxProp, morphSphere, shSin( T ) );


    res = opU( res, mixResult );


    vec4 mirrorBox = objTexBox(
        p, vec3( 3.0, 0.5, -0.5 ),
        vec3( 0.1, 1.1, 1.1 ),
        vec3(10.0, 10.0, 10.0),
        vec3(666.0, 10.0, 10.0)
    );

    res = opU(res, mirrorBox);

    float bgBoxMat = 9447.04;
    vec4 bgBox = objTexBox(
            p, vec3( 7.0, 0.5, -0.5 ),
            vec3( 0.1, 20.1, 20.1 ),
            vec3(bgBoxMat, bgBoxMat, bgBoxMat),
            vec3(bgBoxMat, bgBoxMat, bgBoxMat)
        );
    res = opU(res, bgBox);

    return res;
}

vec3 calcNormal( vec3 pos ){
	float distancePoint = map( pos ).x;
    float aepsilon = 0.00001;
    float x = map(pos + vec3(aepsilon, 0.0, 0.0)).x;
    float y = map(pos + vec3(0.0, aepsilon, 0.0)).x;
    float z = map(pos + vec3(0.0, 0.0, aepsilon)).x;
	return normalize(vec3(x - distancePoint, y - distancePoint, z -distancePoint));
}

vec4 castRay( vec3 ro, vec3 rd ){
    float tmin = 0.01;
    float tmax = FAR_PLANE;
    float prec = 0.001;
    float t = tmin;
    float m = -1.0;
    vec2 tex = vec2(0.0);
    for(int i=0; i < MAX_STEPS; i++){
        vec4 res = map( ro + rd*t );
        if( res.x < prec || t > tmax){
            break;
        }
        t += res.x;
        m = res.y;
        tex = res.zw;
    }
    if(t > tmax){
        m = -1.0;
    }
    return vec4( t, m, tex );
}

vec4 castRayReflect( vec3 ro, vec3 rd ){
    float tmin = 0.01;
    float tmax = FAR_PLANE;
    float prec = 0.001;
    float t = tmin;
    float m = -1.0;
    vec2 tex = vec2(0.0);
    for(int i=0; i < 50; i++){
        vec4 res = map( ro + rd*t );
        if( res.x < prec || t > tmax){
            break;
        }
        t += res.x;
        m = res.y;
        tex = res.zw;
    }
    if(t > tmax){
        m = -1.0;
    }
    return vec4( t, m, tex );
}




float castShadow(vec3 p, vec3 lightSource){
    vec3 ro = p;
    vec3 rd = normalize(lightSource - p);
    vec4 currMat = map( p );
    float rs = 0.0;

    vec4 rayInfo = castRay(ro, rd);

    return rayInfo.y > 0.0 && rayInfo.y != currMat.y ? 1.0 : 0.0;
}


vec4 mapTexture(float m, vec2 tex){
    vec4 col = vec4(0.0);

    col += float(m==1488.0) * vec4( scene( tex, time ), 1.0);

    return col;
}


vec3 renderReflect(vec3 ro, vec3 rd){
    vec3 col = BG_COLOR;
    vec4 texCol = vec4(-1.0);
    vec4 res = castRayReflect( ro, rd );
    float t = res.x;
    float m = res.y;
    vec2 tex = res.zw;
    if(m > 0.0){

        vec3 pos = ro + rd*t;

        vec3 material = vec3( abs(sin(m*88.2 + 198.36)), abs(sin(m*11.2 + 11.36)), abs(sin(m*8228.2 + 3.36)) );

        texCol = mapTexture(m, tex);
        col = texCol.w > 0.0 ? texCol.xyz : material;

        vec3 nor = calcNormal( pos );

        //directional light
        vec3 dirLightV = normalize(vec3(2.0, -1.0, -1.0)); //directional light vector
        vec3 dirLightC = vec3( 1.0 ); //directional light color
        float dif = clamp( dot( nor, -dirLightV ), 0.0, 1.0);
        float shadowAmount = castShadow(pos, pos-dirLightV);
        dif = dif * (1.0-shadowAmount);
        //ambient light
        vec3 ambC = vec3(1.0); // ambient color

        vec3 env = vec3(0.0);
        env += 1.2 * dif * dirLightC;
        env += 0.5 * ambC;
//
        col = clamp( col*env, 0.0, 1.0 );
    }

    return col;
}


vec3 render(vec3 ro, vec3 rd){
    vec3 col = BG_COLOR;
    vec4 texCol = vec4(-1.0);
    vec4 res = castRay( ro, rd );
    float t = res.x;
    float m = res.y;
    vec2 tex = res.zw;
    if(m > 0.0){

        vec3 pos = ro + rd*t;
        vec3 nor = calcNormal( pos );


        vec3 material = vec3( abs(sin(m*88.2 + 198.36)), abs(sin(m*11.2 + 11.36)), abs(sin(m*8228.2 + 3.36)) );

        texCol = mapTexture(m, tex);
        col = texCol.w > 0.0 ? texCol.xyz : material;

        if(m == 666.0){
            return renderReflect(pos, reflect(rd, nor));
        }

        //directional light
        vec3 dirLightV = normalize(vec3(2.0, -1.0, -1.0)); //directional light vector
        vec3 dirLightC = vec3( 1.0 ); //directional light color
        float dif = clamp( dot( nor, -dirLightV ), 0.0, 1.0);
        float shadowAmount = castShadow(pos, pos-dirLightV);
        dif = dif * (1.0-shadowAmount);
        //ambient light
        vec3 ambC = vec3(1.0); // ambient color

        vec3 env = vec3(0.0);
        env += 1.2 * dif * dirLightC;
        env += 0.5 * ambC;
//
        col = clamp( col*env, 0.0, 1.0 );
    }

    return col;
}




mat3 setCamera(vec3 cp, vec3 ct, vec3 cu){ // cp - cam position, ct - cam target, cu - cam up
    vec3 forward = normalize(ct - cp); //dirtection vector
    vec3 right = normalize( cross( forward, cu ) ); // cam right vector
    vec3 upTrue = normalize( cross( right, forward ) ); //another up vector????? dont know for what,
                                             //tutorials doesnt tell anything, just provide formula
    return mat3(right, upTrue, forward);
}

void main(){
    //pixel position
    vec2 uv =  2.0 * gl_FragCoord.xy / resolution.xy - 1.0  ;
    float asp = max(resolution.x, resolution.y) / min(resolution.y, resolution.x);
//    uv.y *= -1.0;

    //mouse position
    vec2 muv = ( mousePos.xy / resolution.xy - 0.5 ) * 2.0 ;
    if( resolution.x > resolution.y ){
       uv.x *= asp;
       muv.x *= asp;
    }else{
       uv.y *= asp;
       muv.y *= asp;
    }

    mat3 camRotation = getVecRotation(vec3(0.0, 1.0, 0.0), sin(time*0.05) * (PI - PI/2.0));

    mat3 cRX = getVecRotation(vec3(1.0, 0.0, 0.0), PI / 4.0 * (2.0 * sin(time*0.1) + 1.0)/2.0);

    float camPosY = shSin( time * 0.1 ) * 3.0 - 0.5 ;

    vec3 camPosition =  vec3( -4.0, 1.8, -2.2 ) * camRotation ;
    vec3 camTarget = vec3( 0.0, 0.0, 0.0 ) ;
    vec3 camUp = vec3( 0.0, 1.0, 0.0 );

    mat3 camTransform = setCamera( camPosition, camTarget, camUp );


    vec3 rd = camTransform * normalize(vec3( uv, 2.0) );
    vec3 color = render( camPosition, rd );


//    color = mix(color, vec3(0.0, 1.0, 0.0), 0.2 * step(uv.x, sin(uv.y)));

    gl_FragColor = vec4(color, 1.0);
}