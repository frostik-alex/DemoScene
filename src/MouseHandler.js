/**
 * Created by Alexey on 10-Oct-15.
 */

var MouseHandler = (function(){
    function MHandler(){
        this._position = [0,0];
    }

    MHandler.prototype.getPosition = function(){
        return this._position;
    };

    MHandler.prototype.initListeners = function ( objectToBind) {
        var me = this;
        objectToBind.addEventListener("mousemove", me.onMouseMove.bind(me));
    };
    
    MHandler.prototype.onMouseMove = function (e) {
        this._position[0] = e.offsetX;
        this._position[1] = e.offsetY;
    };

    return new MHandler();
})();