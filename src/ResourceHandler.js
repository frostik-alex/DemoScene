/**
 * Created by Alexey on 10-Oct-15.
 */


var Resources = (function(){

    function RH(){}

    RH.prototype.loadResources = function( config, callback ){
        var me = this,
            key, info,

            keys = Object.keys(config),

            i, url,

            resources = {};

        function loadSingleResource(url, cb){
            var request = new XMLHttpRequest();
            request.open('POST', url, true);
            request.addEventListener('load', function() {
                cb(request.responseText);
            });
            request.send();
        }

        i = 0;
        key = keys[i];
        info = config[keys[i]];
        url = info.url;
        var cbFunction = function( text ) {
            resources[key] = text;
            i++;
            if (i >= keys.length) {
                if (callback) {
                    me.resources = resources;
                    callback();
                }
                return;
            }

            key = keys[i];
            info = config[keys[i]];
            url = info.url;
            loadSingleResource( url, cbFunction);
        };

        loadSingleResource( url, cbFunction );

    };


    RH.prototype.getResource = function( key ){
        return this.resources[key];
    };


    return new RH();
})();